<p align="center">
<img src="../logo.jpg"/>
</p>


# ems-demo-project

We’re really happy that you are considering joining Wallbox!
We prepared a sample coding exercise below to demonstrate your knowledge of architecture, design patterns, and UI components.


## The Wallbox Energy Management System demo project

From a high-level point of view, the demo app consists of a two screens app. The first screen is a dashboard that holds different widgets of an energy management system [(EMS)](energy-management-system.md). The widgets allow you to observe the different sources of energy and the amount of energy overtime.

Energy sources are obtained through an [API](api-reference.md).

## Requirements

The dashboard screen will have:

* The widget with the amount of energy discharged from the Quasar charger in kWh.
* The widget with the amount of energy charged with the Quasar Charger in kWh.
* The widget with the live data of each different source of energy and the building demand (this should be straightforward from the live mock data).


* The widget with the statistics in form of percentages for each source that supplies the building energy demand. On tapping this widget, user will navigate to the detail screen.

The detail screen will have:

* A chart where all the data is plotted, separated by source of energy, so it can be distinguished in just one sight. The X-axis will contain the time and Y-axis the power in kW. 


## Guidelines

* We like code that is simple, but simple is different from easy.
* Keep in mind the SOLID principles when doing the project
* Testing is very important for us. The app should be easy to test.
* Error scenarios should be taken into consideration.
* Be opinionated on the architecture you use and take your time to make it a reflection of your thought process.

