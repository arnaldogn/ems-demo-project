//
//  EmsDemoTests.swift
//  EmsDemoTests
//
//  Created by Arnaldo Gnesutta on 3/6/22.
//

import XCTest
@testable import EmsDemo
import Combine

class EmsDemoTests: XCTestCase {
    
    var cancellables: Set<AnyCancellable>?
    
    override func setUpWithError() throws {
        cancellables = Set<AnyCancellable>()
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        cancellables = nil
    }

    func testResponseCode_AfterCallingServiceBaseURLEndpoint_ShouldBe200() {
        let dataService = DataService()
        let expectation = expectation(description: "Endpoint is reachable and returns no error")
        
        dataService.baseURL.path = ""
        dataService.baseURL.queryItems = nil
        
        URLSession.shared.dataTask(with: dataService.baseURL.url!) { data, response, error in
            guard error == nil else {
                XCTFail(error?.localizedDescription ?? "")
                return
            }
            let code = (response as? HTTPURLResponse)?.statusCode ?? 0
            if code != 200 { XCTFail("Wrong status code: \(code)") }
            
            expectation.fulfill()
        }.resume()
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testLiveData_AfterDecodingFromJsonExample_ShouldNotBeNull() {
        do {
            let data = LiveData.example.data(using: .utf8)
            let liveData: LiveData? = try data?.decode()
            XCTAssertNotNil(liveData)
        } catch {
            XCTFail("Decoding failed")
        }
    }
    
    func testLiveData_AfterFetchingOnDasboardInit_ShouldNotBeNull() {
        let fakeService = FakeDataService()
        let viewModel = DashboardViewModel(service: fakeService)
        
        let expectation = expectation(description: "Model has a value")
        
        viewModel
            .$liveData
            .dropFirst()
            .sink { _ in
                expectation.fulfill()
            }
            .store(in: &cancellables!)
        
        wait(for: [expectation], timeout: 10)
        XCTAssertNotNil(viewModel.liveData)
    }
}
