//
//  DashboardViewModel.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 05/06/2022.
//

import Foundation

class DashboardViewModel: ObservableObject {
    private let service: DataServiceProtocol
    @Published var liveData: LiveDataModel?
    
    init(service: DataServiceProtocol) {
        self.service = service
        fetchLiveData()
    }
    
    func fetchLiveData() {
        Task.init {
            let liveDataTemp = try await service.fetchLiveData()
            liveData = LiveDataModel(liveDataTemp)
        }
    }
}
