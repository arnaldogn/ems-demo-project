//
//  DetailViewModel.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 7/6/22.
//

import SwiftUI
import Charts

class DetailViewModel: ObservableObject {
    private let service: DataService
    @Published var historicData: [ChartDataEntry] = []
    
    init(service: DataService) {
        self.service = service
    }
    
    func fetchHistoricData() {
        Task.init {
            let historicDataTemp = try await service.fetchHistoricData()
            guard let first = historicDataTemp.first?.timestamp else { return }
            historicData = historicDataTemp.map {
                let value = Calendar.current.dateComponents([.minute], from: first, to: $0.timestamp).minute ?? 0
                return ChartDataEntry(x: Double(value), y: $0.buildingActivePower)
            }
        }
    }
}
