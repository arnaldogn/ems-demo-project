//
//  EmsDemoApp.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 3/6/22.
//

import SwiftUI

@main
struct EmsDemoApp: App {
    let viewModel = DashboardViewModel(service: DataService())
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                DashboardView(viewModel: viewModel)
            }
        }
    }
}
