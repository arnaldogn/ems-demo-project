//
//  HistoricData.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 05/06/2022.
//

import Foundation

struct HistoricData: Decodable {
    let buildingActivePower,
        gridActivePower,
        pvActivePower,
        quasarsActivePower: Double
    let timestamp: Date
    
    enum CodingKeys: String, CodingKey {
        case buildingActivePower = "building_active_power"
        case gridActivePower = "grid_active_power"
        case pvActivePower = "pv_active_power"
        case quasarsActivePower = "quasars_active_power"
        case timestamp
    }
}

struct HistoricDataModel: kWAble {
    private let historicData: HistoricData
    
    var buildingActivePower: String { historicData.buildingActivePower.value }
    var gridActivePower: String { historicData.gridActivePower.value }
    var quasarsActivePower: String { historicData.quasarsActivePower.value }
    
    var timestamp: String {
        ""
    }
}
