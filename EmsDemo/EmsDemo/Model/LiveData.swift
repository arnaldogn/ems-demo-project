//
//  LiveData.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 05/06/2022.
//

import Foundation

protocol kWAble { }

struct LiveData: Decodable {
    let solarPower,
        quasarsPower,
        gridPower,
        buildingDemand,
        systemSoc,
        totalEnergy,
        currentEnergy: Double
    
    enum CodingKeys: String, CodingKey {
        case solarPower = "solar_power"
        case quasarsPower = "quasars_power"
        case gridPower = "grid_power"
        case buildingDemand = "building_demand"
        case systemSoc = "system_soc"
        case totalEnergy = "total_energy"
        case currentEnergy = "current_energy"
    }
    
    static var example = """
    {
        "solar_power": 7.827,
        "quasars_power": -38.732,
        "grid_power": 80.475,
        "building_demand": 127.03399999999999,
        "system_soc": 48.333333333333336,
        "total_energy": 960,
        "current_energy": 464.0
    }
    """
}

struct LiveDataModel {
    private let liveData: LiveData
    
    init(_ liveData: LiveData)  {
        self.liveData = liveData
    }
    
    var solarPower: String { liveData.solarPower.value }
    var quasarsPower: String { liveData.quasarsPower.value }
    var gridPower: String { liveData.gridPower.value }
    var buildingDemand: String { liveData.buildingDemand.value }
    var systemSoc: String { liveData.systemSoc.value }
    var totalEnergy: String { liveData.totalEnergy.value }
    var currentEnergy: String { liveData.currentEnergy.value }
    var solarPowerPercentage: String { "\(Double(liveData.solarPower/liveData.currentEnergy).percentage)" }
    var gridPowerPercentage: String { "\(Double(liveData.gridPower/liveData.currentEnergy).percentage)" }
    
    var dischargedEnergy: String {
        liveData.quasarsPower < 0 ? Double(liveData.quasarsPower * -1).value : "0 kW"
    }
    
    var energyChargedInQuasars: String {
        liveData.quasarsPower > 0 ? Double(liveData.quasarsPower * -1).value : "0 kW"
    }
    
    var quasarsPowerPercentage: String {
        liveData.quasarsPower > 0 ? Double(liveData.quasarsPower/liveData.currentEnergy).percentage : "0%"
    }
}

extension Double: kWAble {
    var value: String {
        "\(self) kW"
    }
    
    var percentage: String {
        String(format: "%.2f%%", self)
    }
}

