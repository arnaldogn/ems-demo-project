//
//  DataService.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 05/06/2022.
//

import Foundation

enum EmsError: Error {
    case decoding
    case wrongURL
    case noData
}

protocol DataServiceProtocol {
    func fetchLiveData() async throws -> LiveData
    func fetchHistoricData() async throws -> [HistoricData]
}

final class DataService: DataServiceProtocol {
    private enum Kind {
        case live, historic
        
        var filename: String {
            switch self {
            case .live:
                return "live_data.json"
            case .historic:
                return "historic_data.json"
            }
        }
    }
    
    lazy var baseURL: URLComponents = {
       var component = URLComponents()
        component.scheme = "https"
        component.host = "gitlab.com"
        component.path = "/arnaldogn/ems-demo-project/-/raw/main/"
        component.queryItems = [URLQueryItem(name: "inline", value: "false")]
        return component
    }()
    
    @MainActor
    func fetchLiveData() async throws -> LiveData {
        return try await fetchData(.live)
    }
    
    @MainActor
    func fetchHistoricData() async throws -> [HistoricData] {
        return try await fetchData(.historic)
    }
    
    private func fetchData<T: Decodable>(_ kind: Kind) async throws -> T {
        baseURL.path += kind.filename
        guard let url = baseURL.url else { throw EmsError.wrongURL }
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            return try data.decode()
        }
        catch {
            throw EmsError.noData
        }
    }    
}

class FakeDataService: DataServiceProtocol {
    func fetchLiveData() async throws -> LiveData {
        return try LiveData.example.data(using: .utf8)!.decode()
    }
    
    func fetchHistoricData() async throws -> [HistoricData] {
        return []
    }
}
