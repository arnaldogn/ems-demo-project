//
//  Data+Extensions.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 7/6/22.
//

import Foundation

extension Data {
    func decode<T: Decodable>() throws -> T {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        do {
            return try decoder.decode(T.self, from: self)
        }
        catch {
            throw EmsError.decoding
        }
    }
}
