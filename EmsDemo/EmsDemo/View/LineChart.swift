//
//  LineChart.swift
//  Charts-SwiftUI
//
//  Created by Stewart Lynch on 2020-11-20.
//

import Charts
import SwiftUI

struct LineChart: UIViewRepresentable {
    let lineChart = LineChartView()
    var entriesIn : [ChartDataEntry]
    
    func makeUIView(context: Context) -> LineChartView {
        return lineChart
    }

    func updateUIView(_ uiView: LineChartView, context: Context) {
        setChartData(uiView)
        configureChart(uiView)
        formatXAxis(xAxis: uiView.xAxis)
        uiView.notifyDataSetChanged()
    }
    
    func setChartData(_ lineChart: LineChartView) {
        let dataSetIn = LineChartDataSet(entries: entriesIn)
        let dataSets: [LineChartDataSet] = [dataSetIn]
        let lineChartData = LineChartData(dataSets: dataSets)
        lineChart.data = lineChartData
        formatDataSet(dataSet: dataSetIn, label: "Historic building active power - kW", color: .blue)
    }
    
    func formatDataSet(dataSet: LineChartDataSet, label: String, color: UIColor) {
        dataSet.label = label
        dataSet.colors = [color]
        dataSet.valueColors = [color]
        dataSet.circleColors = [color]
        dataSet.circleRadius = 1
        let format = NumberFormatter()
        format.numberStyle = .none
        dataSet.valueFormatter = DefaultValueFormatter(formatter: format)
        dataSet.valueFont = UIFont.systemFont(ofSize: 12)
    }
    
    func formatXAxis(xAxis: XAxis) {
        xAxis.labelPosition = .bottom
    }

    func configureChart(_ lineChart: LineChartView) {
        lineChart.noDataText = "No Data"
        lineChart.drawGridBackgroundEnabled = true
        lineChart.gridBackgroundColor = UIColor.tertiarySystemFill
        lineChart.drawBordersEnabled = true
        lineChart.rightAxis.enabled = false
        lineChart.setScaleEnabled(true)
        if lineChart.scaleX == 1.0 {
            lineChart.zoom(scaleX: 1.5, scaleY: 1, x: 0, y: 0)
        }
        lineChart.animate(xAxisDuration: 0, yAxisDuration: 0.5, easingOption: .linear)
    }
}

struct LineChart_Previews: PreviewProvider {
    static var previews: some View {
        LineChart(
            entriesIn: [])
            .frame(height: 400)
            .padding(.horizontal)
    }
}
