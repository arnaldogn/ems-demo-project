//
//  DashboardView.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 05/06/2022.
//

import SwiftUI

struct DashboardView: View {
    @StateObject var viewModel: DashboardViewModel
    var body: some View {
        if let liveData = viewModel.liveData {
            List {
                Section(header: Text("Building supply from Quasars")) {
                    Text(liveData.dischargedEnergy)
                }
                Section(header: Text("Quasars charge")) {
                    Text(liveData.energyChargedInQuasars)
                }
                Section(header: Text("Live Data - Power")) {
                    Text("Grid: \(liveData.gridPower)")
                    Text("Solar: \(liveData.solarPower)")
                    Text("Quasars: \(liveData.quasarsPower)")
                }
                Section(header: Text("Statistics")) {
                    Text("Grid: \(liveData.gridPowerPercentage)")
                    Text("Solar: \(liveData.solarPowerPercentage)")
                    Text("Quasars: \(liveData.quasarsPowerPercentage)")
                }
                Section {
                    NavigationLink(destination: DetailView(viewModel: DetailViewModel(service: DataService()))) {
                        Text("View details")
                    }
                }
            }
            .listStyle(.insetGrouped)
            .navigationTitle("EMS")
        } else {
            EmptyView()
        }
    }
}

struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView(viewModel: DashboardViewModel(service: DataService()))
    }
}
