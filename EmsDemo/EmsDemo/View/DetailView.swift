//
//  DetailView.swift
//  EmsDemo
//
//  Created by Arnaldo Gnesutta on 6/6/22.
//

import SwiftUI
import Charts

struct DetailView: View {
    @StateObject var viewModel: DetailViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            LineChart(entriesIn: viewModel.historicData)
                .padding()
            Spacer()
        }
        .onAppear {
            viewModel.fetchHistoricData()
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(viewModel: DetailViewModel(service: DataService()))
    }
}
